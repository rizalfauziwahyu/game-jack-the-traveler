# Petualangan Mas Teja

## Petualangan Mas Teja merupakan sebuah game edukasi belajar bahasa jawa dasar untuk anak usia sekolah dasar. Aplikasi bermain ini dilengkapi dengan berbagai materi yang dibalut dengan beberapa mini game yang bisa digunakan sebagai media Edutainment atau belajar sambil bermain bagi anak-anak.
