﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickableLetter : MonoBehaviour, IPointerClickHandler
{
    char randomLetter;
    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log($"Clicked on letter {randomLetter}");
        if (randomLetter == GameController.Letter)
        {
            GetComponent<TMP_Text>().color = Color.green;
            GameController.HandleCorrectLetterClick();
            this.enabled = false;   
        }
    }

    internal void SetLetter(char letter)
    {
        randomLetter = letter;
        GetComponent<TMP_Text>().text = letter.ToString().ToUpper();
    }
}
