﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizUI : MonoBehaviour
{
    [SerializeField] private QuizManager quizManager;
    [SerializeField] private Text questionText, scoreText, timerText;
    [SerializeField] private List<Image> lifeImageList;
    [SerializeField] private GameObject gameOverPanel, beforeMenuPanel, gameMenuPanel;
    [SerializeField] private Image questionImage;
    [SerializeField] private List<Button> options, uiButton;
    [SerializeField] private Color correcrCol, wrongCol, normalCol;

    private Question questions;
    private bool answered;

    public Text ScoreText { get { return scoreText; } }
    public Text TimerText { get { return timerText; } }

    public GameObject GameOverPanel { get { return GameOverPanel; } }

    // Start is called before the first frame update
    void Awake()
    {
        for (int i = 0; i < options.Count; i++ )
        {
            Button localBtn = options[i];
            localBtn.onClick.AddListener(() => OnClick(localBtn));
        }
        for (int i = 0; i < uiButton.Count; i++)
        {
            Button localBtn = uiButton[i];
            localBtn.onClick.AddListener(() => OnClick(localBtn));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetQuestion(Question questions)
    {
        this.questions = questions;

        switch (questions.questionType)
        {
            case QuestionType.TEXT:
                questionImage.transform.parent.gameObject.SetActive(false);
                break;
            case QuestionType.IMAGE:
                QuestPanel();
                questionImage.transform.gameObject.SetActive(true);
                questionImage.sprite = questions.questionImage;
                break;
        }

        questionText.text = questions.questionInfo;
        List<string> answerList = ShuffleList.ShuffleListItems<string>(questions.options);

        for (int i = 0; i < options.Count; i++)
        {
            options[i].GetComponentInChildren<Text>().text = answerList[i];
            options[i].name = answerList[i];
            options[i].image.color = normalCol;
        }

        answered = false;
    }

    void QuestPanel()
    {
        questionImage.transform.parent.gameObject.SetActive(true);
        questionImage.transform.gameObject.SetActive(false);
    }

    private void OnClick(Button btn)
    {
        if (quizManager.GMStatus == GameStatus.Playing)
        {
            if (!answered)
            {
                answered = true;
                bool val = quizManager.Answer(btn.name);

                if (val)
                {
                    btn.image.color = correcrCol;
                }
                else
                {
                    btn.image.color = wrongCol;
                }
            }
        }

        switch (btn.name)
        {
            case "Play":
                break;
        }
    }

    public void ReduceLife(int Index)
    {
        lifeImageList[Index].color = wrongCol;
    }
}
