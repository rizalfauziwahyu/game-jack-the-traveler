﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizManager : MonoBehaviour
{
    [SerializeField] private QuizUI quizUI;
    [SerializeField] private QuizDataScriptable quizData;
    [SerializeField]
    private List<Question> question;
    [SerializeField] private float timeLimit = 60f;

    private Question selectedQuestion;

    private int scoreCount = 0;
    private float curentTimer;
    private int lifeRemaining = 3;

    private GameStatus gameStatus = GameStatus.Next;

    public GameStatus GMStatus { get { return gameStatus; } }

    // Start is called before the first frame update
    public void StartGame()
    {
        scoreCount = 0;
        curentTimer = timeLimit;
        lifeRemaining = 3;

        question = quizData.questions;

        //question = new List<Question>();

        SelectQuestion();

        gameStatus = GameStatus.Playing;
    }

    void SelectQuestion()
    {
        int value = UnityEngine.Random.Range(0, question.Count);
        selectedQuestion = question[value];

        quizUI.SetQuestion(selectedQuestion);

        question.RemoveAt(value);
    }

    private void Update()
    {
        if (gameStatus == GameStatus.Playing)
        {
            curentTimer -= Time.deltaTime;
            SetTimer(curentTimer);
        }
    }

    private void SetTimer(float val)
    {
        TimeSpan time = TimeSpan.FromSeconds(val);
        quizUI.TimerText.text = "Time : " + time.ToString("mm ':'ss");

        if (curentTimer <= 0)
        {
            quizUI.GameOverPanel.SetActive(true);
        }
    }

    public bool Answer(string answered)
    {
        bool correctAnswer = false;

        if (answered == selectedQuestion.CorrectAnswer)
        {
            correctAnswer = true;
            scoreCount += 1;
            quizUI.ScoreText.text = "Score : " + scoreCount;
        }
        else
        {
            lifeRemaining--;
            quizUI.ReduceLife(lifeRemaining);

            if (lifeRemaining <= 0)
            {
                quizUI.GameOverPanel.SetActive(true);
            }
        }

        if (gameStatus == GameStatus.Playing)
        {
            if (question.Count > 0)
            {
                Invoke("SelectQuestion", 0.4f);
            }
            else
            {
                quizUI.GameOverPanel.SetActive(true);
            }
        }

        return correctAnswer;
    }
}

[System.Serializable]
public class Question
{
    public string questionInfo;
    public QuestionType questionType;
    public Sprite questionImage;
    public List<string> options;
    public string CorrectAnswer;
}

[System.Serializable]
public enum QuestionType
{ 
    TEXT,
    IMAGE
}

[System.Serializable]
public enum GameStatus
{ 
    Next,
    Playing
}
