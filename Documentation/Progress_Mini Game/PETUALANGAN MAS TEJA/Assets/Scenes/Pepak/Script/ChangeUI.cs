﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class ChangeUI : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Button button = GetComponent<Button>();
        button.onClick.AddListener(() => {

            PopUp popup = UIController.Instance.CreatePopUp();
            popup.Init(UIController.Instance.MainCanvas,
                "BI",
                "BJ",
                "Ayam",
                "Pithik"
                );

        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
