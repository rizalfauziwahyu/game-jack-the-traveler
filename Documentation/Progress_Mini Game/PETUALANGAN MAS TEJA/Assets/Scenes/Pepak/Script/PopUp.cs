﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class PopUp : MonoBehaviour
{
    [SerializeField] Button ExitButton;
    [SerializeField] Button SoundButtonIndo;
    [SerializeField] Button SoundButtonJawa;
    [SerializeField] Image Symbol;
    [SerializeField] Text BI;
    [SerializeField] Text BJ;
    [SerializeField] Text T_Indo;
    [SerializeField] Text T_Jawa;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Init(Transform canvas, String Text_BI, String Text_BJ, String Kata_BI, String Kata_BJ)
    {
        T_Indo.text = Kata_BI;
        T_Jawa.text = Kata_BJ;
        BI.text = Text_BI;
        BJ.text = Text_BJ;

        transform.SetParent(canvas);
        transform.localScale = Vector3.one;

        ExitButton.onClick.AddListener(() => {

            GameObject.Destroy(this.gameObject);
        
        });
    }
}
