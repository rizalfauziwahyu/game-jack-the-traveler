﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    GameObject target;
    public float speed;
    private Vector2 Move;
    private Transform _Player;

    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        _Player = GameObject.FindGameObjectWithTag("Player").transform;

        //Vector2 moveDir = (_Player.transform.position - transform.position).normalized * speed;
        //rb.velocity = new Vector2(moveDir.x, moveDir.y);

        //Destroy(this.gameObject, 2);
    }

    // Update is called once per frame
    void Update()
    {
        LookAtPlayer();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        }
    }



    void LookAtPlayer()
    {
        Vector3 direct = _Player.position - transform.position;
        float angle = Mathf.Atan2(direct.y, direct.x) * Mathf.Rad2Deg - 90f;
        rb.rotation = angle;
        direct.Normalize();
        Move = direct;
    }

    private void FixedUpdate()
    {
        MoveCharacter(Move);
    }

    void MoveCharacter(Vector2 direction)
    {
        rb.MovePosition((Vector2)transform.position + (direction * speed * Time.deltaTime));
    }
}
