﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class BarrierWeapon : MonoBehaviour
{
    public float offset;
    public GameObject Shield;
    public Transform ShieldPos;
    public bool ActiveShield;
    //[SerializeField] float delay = 2f;

    private void Start()
    {
        ActiveShield = false;   
    }

    void Update()
    {
        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rotZ - offset);

        if (Input.GetKeyDown("space"))
        {
            //Instantiate(Shield, ShieldPos.position, ShieldPos.rotation);

            ActiveShield = true;

            if (ActiveShield == true)
            {
                Shield.SetActive(true);
                ManaBarScript.instance.UseMana(15);

                if (ActiveShield == true)
                {
                    StartCoroutine(GetDelayActive());
                }
            }
            else
            {
                Shield.SetActive(false);
            }
        }
    }

    IEnumerator GetDelayActive()
    {
        yield return new WaitForSeconds(3);
        Shield.SetActive(false);
    }
}
