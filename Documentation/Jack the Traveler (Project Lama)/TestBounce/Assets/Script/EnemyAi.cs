﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class EnemyAi : MonoBehaviour
{
    //private EnemyPathfindingMovement pathfindingMovement;
    private Vector3 startingPoint;
    private Vector3 roamPosition;

    private void Awake()
    {
        //pathfindingMovement = GetComponent<EnemyPathfindingMovement>();
    }

    // Start is called before the first frame update
    void Start()
    {
        startingPoint = transform.position;
        roamPosition = GetRoamingPos();
    }

    public static Vector3 GetRandomDir()
    {
        return new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f)).normalized;
    }

    // Update is called once per frame
    void Update()
    {
        //pathfindingMovement.MoveTo(roamPosition);
        float reachedPositionDistance = 1f;
        if (Vector3.Distance(transform.position, roamPosition) < reachedPositionDistance);
        {
            // Reach Roam Pos
            roamPosition = GetRoamingPos();
        }
    }

    private Vector3 GetRoamingPos()
    {
        return startingPoint + GetRandomDir() * Random.Range(10f, 70f);
    }
}
