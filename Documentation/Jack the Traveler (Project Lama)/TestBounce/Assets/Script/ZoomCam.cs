﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomCam : MonoBehaviour
{
    public bool ZoomActive;
    public Vector3[] Target;
    public Camera cam;
    public float Speed;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    public void LateUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ZoomActive = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            ZoomActive = false;
        }

        if (ZoomActive == true)
        {
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, 12, Speed);
        }
        else if(ZoomActive == false)
        {
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, 6, Speed);
        }
    }

    
}
