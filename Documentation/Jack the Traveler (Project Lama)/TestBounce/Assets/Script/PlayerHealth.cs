﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int Maxhealth;
    public int CurrentHealth;
    public HealthSystem myHealth;

    // Start is called before the first frame update
    void Start()
    {
        CurrentHealth = Maxhealth;
        myHealth.SetMaxhealth(Maxhealth);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void TakeDemageAndDie(int damage)
    {
        if (CurrentHealth <= 0)
        {
            Time.timeScale = 0f;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ammo")
        {
            Debug.Log("Kena Kau");
            CurrentHealth -= 10;
            myHealth.setHealth(CurrentHealth);
        }
    }



}
