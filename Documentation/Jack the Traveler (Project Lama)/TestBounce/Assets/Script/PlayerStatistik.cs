﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Player Stasts")]
public class PlayerStatistik : MonoBehaviour
{
    public int playerHealth;
    public int playerRank;
    public int playerLevel;
    public string playerName;
}
