﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragNshot : MonoBehaviour
{
    public float power = 10f;
    public Rigidbody2D rb;

    public Vector2 minPow;
    public Vector2 maxPow;
    Line lineStats;

    ZoomCam zoom;

    Camera cam;
    Vector2 force;
    Vector3 startPoint;
    Vector3 endPoint;

    bool SlowOn = false;
    public TimeManager TM;

    private void Start()
    {
        cam = Camera.main;
        lineStats = GetComponent<Line>();
    }

    private void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            SlowOn = true;

            startPoint = cam.ScreenToWorldPoint(Input.mousePosition);
            startPoint.z = 15;

            if (SlowOn == true)
            {
                TM.DoSlowmo();
            }
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 currentPoint = cam.ScreenToWorldPoint(Input.mousePosition);
            currentPoint.z = 15;
            lineStats.RenderLine(startPoint, currentPoint);
        }

        if (Input.GetMouseButtonUp(0))
        {
            SlowOn = false;

            endPoint = cam.ScreenToWorldPoint(Input.mousePosition);
            endPoint.z = 15;

            force = new Vector2(Mathf.Clamp(startPoint.x - endPoint.x, minPow.x, maxPow.x), Mathf.Clamp(startPoint.y - endPoint.y, minPow.y, maxPow.y));
            rb.AddForce(force * power, ForceMode2D.Impulse);
            lineStats.endline();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        EnemyController enemy = collision.collider.GetComponent<EnemyController>();
        if (enemy != null)
        {
            foreach (ContactPoint2D point in collision.contacts)
            {
                Debug.DrawLine(point.point, point.point + point.normal);
            }
        }
    }
}
