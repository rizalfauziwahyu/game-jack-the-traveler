﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaBarScript : MonoBehaviour
{
    public Slider ManaBar;

    private int MaxMana = 100;
    private int currentMana;

    public static ManaBarScript instance;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        currentMana = MaxMana;
        ManaBar.maxValue = MaxMana;
        ManaBar.value = currentMana;
    }

    public void UseMana(int amount) {
        if (currentMana - amount >= 0)
        {
            currentMana -= amount;
            ManaBar.value = currentMana;
        }
        else
        {
            Debug.Log("Gak Cukup Bosss...");
        }
    }
}
