﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    public const int gridRow = 2;
    public const int gridCol = 5;
    public const float offsetX = 4f;
    public const float offsetY = 5f;

    [SerializeField] private MainCard oriCard;
    [SerializeField] private Sprite[] images;
    [SerializeField] private TextMesh correctLebel;

    private int Correct = 0;
    private MainCard _firstRevealed;
    private MainCard _secondRevealed;

    private void Start()
    {
        Vector3 startPos = oriCard.transform.position;

        int[] Number = { 0, 0, 1, 1, 2, 2, 3, 3, 4, 4 };
        Number = ShuffleArray(Number);

        for (int i = 0; i < gridCol; i++)
        {
            for (int j = 0; j < gridRow; j++)
            {
                MainCard card;
                if (i == 0 && j == 0)
                {
                    card = oriCard;
                }
                else
                {
                    card = Instantiate(oriCard) as MainCard;
                }

                int index = j * gridCol + i;
                int id = Number[index];
                card.ChangeSprite(id, images[id]);

                float posX = (offsetX * i) + startPos.x;
                float posY = (offsetY * j) + startPos.y;
                card.transform.position = new Vector3(posX, posY, startPos.z);
            }
        }
    }

    private int[] ShuffleArray(int[] Number)
    {
        int[] newArray = Number.Clone() as int[];
        for (int i = 0; i < newArray.Length; i++)
        {
            int tmp = newArray[i];
            int r = Random.Range(i, newArray.Length);
            newArray[i] = newArray[r];
            newArray[r] = tmp;
        }
        return newArray;
    }

    public bool canReveal
    {
        get
        {
            return _secondRevealed == null;
        }
    }

    public void CardRevealed(MainCard card)
    {
        if (_firstRevealed == null)
        {
            _firstRevealed = card;
        }
        else
        {
            _secondRevealed = card;
            StartCoroutine(CheckMatch());
        }
    }

    private IEnumerator CheckMatch()
    {
        if (_firstRevealed.ID == _secondRevealed.ID)
        {
            Correct++;
            correctLebel.text = "Correct : " + Correct;
        }
        else
        {
            yield return new WaitForSeconds(0.5f);

            _firstRevealed.Unreveal();
            _secondRevealed.Unreveal();
        }

        _firstRevealed = null;
        _secondRevealed = null;
    }
}
