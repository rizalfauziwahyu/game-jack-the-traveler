﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCard : MonoBehaviour
{
    [SerializeField] private SceneController control;
    [SerializeField] private GameObject Card_Back;

    private int _ID;
    public int ID
    {
        get
        {
            return _ID;
        }
    }

    public void ChangeSprite(int ID, Sprite Image) {
        _ID = ID;
        GetComponent<SpriteRenderer>().sprite = Image;
    }

    public void Unreveal()
    {
        Card_Back.SetActive(true);
    }

    public void OnMouseDown()
    {
        if (Card_Back.activeSelf && control.canReveal)
        {
            Card_Back.SetActive(false);
            control.CardRevealed(this);
        }
    }
}
