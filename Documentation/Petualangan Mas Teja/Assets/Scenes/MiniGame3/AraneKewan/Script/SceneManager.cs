﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public GameObject lettreOne, lettreTwo, lettreThree, lettreFour, lettreFive, BoxOne, BoxTwo, BoxThree, BoxFour, BoxFive;

    Vector3 lettreOneIni, lettreTwoIni, lettreThreeIni, lettreFourIni, lettreFiveIni, lettreSixIni, lettreSevenIni, lettreEightIni;

    bool oneCorrect, twoCorrect, threeCorrect, fourCorrect, fiveCorrect = false;

    Vector3 iniScaleLettreOne, iniScaleLettreTwo, iniScaleLettreThree, iniScaleLettreFour, iniScaleLettreFive;

    /*
    public AudioSource source;
    public AudioClip[] correct;
    public AudioClip incorrect;
    public AudioClip buttonDrop;
    public AudioClip reload;
    */

    void Start()
    {
        lettreOneIni = lettreOne.transform.position;
        lettreTwoIni = lettreTwo.transform.position;
        lettreThreeIni = lettreThree.transform.position;
        lettreFourIni = lettreFour.transform.position;
        lettreFiveIni = lettreFive.transform.position;

        iniScaleLettreOne = lettreOne.transform.localScale;
        iniScaleLettreTwo = lettreTwo.transform.localScale;
        iniScaleLettreThree = lettreThree.transform.localScale;
        iniScaleLettreFour = lettreFour.transform.localScale;
        iniScaleLettreFive = lettreFive.transform.localScale;

        // blockPanel.SetActive(false);
    }

    //*****************************************Drag

    public void DragLettreOne()
    {
        lettreOne.transform.position = Input.mousePosition;
    }
    public void DragLettreTwo()
    {
        lettreTwo.transform.position = Input.mousePosition;
    }
    public void DragLettreThree()
    {
        lettreThree.transform.position = Input.mousePosition;
    }
    public void DragLettreFour()
    {
        lettreFour.transform.position = Input.mousePosition;
    }
    public void DragLettreFive()
    {
        lettreFive.transform.position = Input.mousePosition;
    }
    //****************************************Drop

    public void DropLettreOne()
    {
        float Distance = Vector3.Distance(lettreOne.transform.position, BoxOne.transform.position);
        if (Distance < 50)
        {
            lettreOne.transform.localScale = BoxOne.transform.localScale;
            lettreOne.transform.position = BoxOne.transform.position;
            oneCorrect = true;
            BoxOne.name = lettreOne.name;
            //source.clip = buttonDrop;
            //source.Play();
        }
        else
        {
            lettreOne.transform.position = lettreOneIni;
            //source.clip = reload;
            //source.Play();
        }

    }

    public void DropLettreTwo()
    {
        float Distance = Vector3.Distance(lettreTwo.transform.position, BoxTwo.transform.position);
        if (Distance < 50)
        {
            lettreTwo.transform.localScale = BoxTwo.transform.localScale;
            lettreTwo.transform.position = BoxTwo.transform.position;
            twoCorrect = true;
            BoxTwo.name = lettreTwo.name;
            //source.clip = buttonDrop;
            //source.Play();
        }
        else
        {
            lettreTwo.transform.position = lettreTwoIni;
            //source.clip = reload;
            //source.Play();
        }

    }

    public void DropLettreThree()
    {
        float Distance = Vector3.Distance(lettreThree.transform.position, BoxThree.transform.position);
        if (Distance < 50)
        {
            lettreThree.transform.localScale = BoxThree.transform.localScale;
            lettreThree.transform.position = BoxThree.transform.position;
            threeCorrect = true;
            BoxThree.name = lettreThree.name;
            //source.clip = buttonDrop;
            //source.Play();
        }
        else
        {
            lettreThree.transform.position = lettreThreeIni;
            //source.clip = reload;
            //source.Play();
        }
    }

    public void DropLettreFour()
    {
        float Distance = Vector3.Distance(lettreFour.transform.position, BoxFour.transform.position);
        if (Distance < 50)
        {
            lettreFour.transform.localScale = BoxFour.transform.localScale;
            lettreFour.transform.position = BoxFour.transform.position;
            fourCorrect = true;
            BoxFour.name = lettreFour.name;
            //source.clip = buttonDrop;
            //source.Play();
        }
        else
        {
            lettreFour.transform.position = lettreFourIni;
            //source.clip = reload;
            //source.Play();
        }

    }

    public void DropLettreFive()
    {
        float Distance = Vector3.Distance(lettreFive.transform.position, BoxFive.transform.position);
        if (Distance < 50)
        {
            lettreFive.transform.localScale = BoxFive.transform.localScale;
            lettreFive.transform.position = BoxFive.transform.position;
            fiveCorrect = true;
            BoxFive.name = lettreFive.name;
            //source.clip = buttonDrop;
            //source.Play();
        }
        else
        {
            lettreFive.transform.position = lettreFiveIni;
            //source.clip = reload;
            //source.Play();
        }
    }

    void Update()
    {
        if (oneCorrect && twoCorrect && threeCorrect && fourCorrect && fiveCorrect)
        {
            Debug.Log("Kamu Berhasil....!!!");
        }
    }
}
