﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class PopUpKewan : MonoBehaviour
{
    public Canvas canvas;
    public bool Active = false;

    public void popup()
    {
        if (Active == false)
        {
            Active = true;
            canvas.enabled = true;
        }
        else
        {
            Active = false;
            canvas.enabled = false;
        }
    }
}
