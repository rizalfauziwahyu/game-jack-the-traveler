﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController Instance;
    public Transform MainCanvas;

    // Start is called before the first frame update
    void Start()
    {
        if (Instance != null)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }

        Instance = this;
    }

    public PopUp CreatePopUp()
    {
        GameObject PopUpGo = Instantiate(Resources.Load("Prefab/PopUp_Ayam") as GameObject);
        return PopUpGo.GetComponent<PopUp>();
    }
}
