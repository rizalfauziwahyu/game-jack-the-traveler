﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static char Letter = 'a';
    private static int correctClicks;
    public static int correctAnswers = 5;

    private void OnEnable()
    {
        GenerateBoard();
    }

    private void GenerateBoard()
    {
        var clickables = FindObjectsOfType<ClickableLetter>();
        List<char> charsList = new List<char>();

        for (int i = 0; i < correctAnswers; i++)
        {
            charsList.Add(Letter);
        }

        for (int i = correctAnswers; i < clickables.Length; i++)
        {
            var chosenLetter = ChooseInvalidRandomLetter();
            charsList.Add(chosenLetter);
        }

        charsList = charsList
            .OrderBy(t => UnityEngine.Random.Range(0, 10000))
            .ToList();

        for (int i = 0; i < clickables.Length; i++)
        {
            clickables[i].SetLetter(charsList[i]);
        }
    }

    internal static void HandleCorrectLetterClick()
    {
        correctClicks++;
        if (correctClicks >= correctAnswers) 
        {
            
        }
    }

    private char ChooseInvalidRandomLetter()
    {
        int a = UnityEngine.Random.Range(0, 26);
        var randomLetter = (char)('a' + a);

        while (randomLetter == Letter)
        {
            a = UnityEngine.Random.Range(0, 26);
            randomLetter = (char)('a' + a);
        }

        if (randomLetter == Letter)
        {
            return ChooseInvalidRandomLetter();
        }

        return randomLetter;
    }
}
