﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour
{
    public GameObject loadingSreen;
    public Slider slider;
    public void Loadlevel(string sceneToLoad) 
    {
        StartCoroutine(LoadAsyncronously(sceneToLoad));
    }

    IEnumerator LoadAsyncronously(string scene)
    {
        loadingSreen.SetActive(true);
        yield return new WaitForSeconds(0.5f);

        AsyncOperation operation = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(scene);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;
            yield return null;
        }

        loadingSreen.SetActive(false);

    }

}
