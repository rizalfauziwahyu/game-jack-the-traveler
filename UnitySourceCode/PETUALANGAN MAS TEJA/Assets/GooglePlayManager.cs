﻿using System;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using UnityEngine;
using UnityEngine.UI;

public class GooglePlayManager : MonoBehaviour
{
    public static GooglePlayManager instance { get; private set; }

    public static PlayGamesPlatform platform;

    public GameObject signIn;
    public Sprite connectedImage;
    public Sprite disconnectedImage;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }


    private void Start()
    {
        // Google Play Service
        if (platform == null)
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true;
            platform = PlayGamesPlatform.Activate();
            OnConnectionResponse(PlayGamesPlatform.Instance.localUser.authenticated);
        }

        Social.Active.localUser.Authenticate((bool success) =>
        {
            OnConnectionResponse(success);
        });
    }



    // Google Play Services
    public void OnConnectClick()
    {
        if (Social.Active.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.SignOut();
            OnConnectionResponse(Social.Active.localUser.authenticated);
        }
        else
        {
            Social.Active.localUser.Authenticate((bool success) =>
            {
                OnConnectionResponse(success);
            });
        }

    }


    private void OnConnectionResponse(bool authenticated)
    {
        if (authenticated)
        {
            UnlockAchievement(GPGSIds.achievement_login);
            signIn.GetComponent<Image>().sprite = connectedImage;
            OpenSave(false);
        }
        else
        {
            signIn.GetComponent<Image>().sprite = disconnectedImage;
        }
    }

    public void OnAchievementClick()
    {
        if (Social.localUser.authenticated)
        {
            Social.ShowAchievementsUI();
        }
    }

    public void UnlockAchievement(string achievementID)
    {
        Social.ReportProgress(achievementID, 100.0f, (bool success) =>
        {
            Debug.Log("Achievement Unlocked" + success.ToString());
        });
    }

    // Leaderboard
    public void OnLeaderboardClick()
    {
        if (Social.localUser.authenticated)
        {
            Social.ShowLeaderboardUI();
        }
    }

    public void ReportScore(int score)
    {
        Social.ReportScore(score, GPGSIds.leaderboard_high_score, (bool success) =>
        {
            Debug.Log("Reportes score to leaderboard " + success.ToString());
        });
    }

    // Cloud Saving
    private bool isSaving = false;
    public void OpenSave(bool saving)
    {
        Debug.Log("Open Save");

        if (Social.localUser.authenticated)
        {
            isSaving = saving;
            ((PlayGamesPlatform)Social.Active).SavedGame.OpenWithAutomaticConflictResolution("MasTeja", GooglePlayGames.BasicApi.DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, SaveGameOpened);
        }
    }

    private void SaveGameOpened(SavedGameRequestStatus status, ISavedGameMetadata meta)
    {
        Debug.Log("SaveGameOpened");
        if (status == SavedGameRequestStatus.Success)
        {
            if (isSaving) // Writing
            {
                string saveString = SaveSystem.Load();
                if (saveString != null)
                {
                    byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(saveString);
                    SavedGameMetadataUpdate update = new SavedGameMetadataUpdate.Builder().WithUpdatedDescription("Save at " + DateTime.Now.ToString()).Build();

                    ((PlayGamesPlatform)Social.Active).SavedGame.CommitUpdate(meta, update, data, SaveUpdate);
                    Debug.Log("Game Saved To The Google Play Service");
                }
                else
                {
                    Debug.Log("No Save!");
                }

            }
            else // Reading
            {
                ((PlayGamesPlatform)Social.Active).SavedGame.ReadBinaryData(meta, SaveRead);
            }
        }
    }

    // Load
    private void SaveRead(SavedGameRequestStatus status, byte[] data)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            string saveData = System.Text.ASCIIEncoding.ASCII.GetString(data);
            if (saveData != null)
            {
                SaveObject saveObject = JsonUtility.FromJson<SaveObject>(saveData);

                PlayerPrefs.SetInt("lv1", saveObject.lv1);
                PlayerPrefs.SetInt("lv2", saveObject.lv2);
                PlayerPrefs.SetInt("lv3", saveObject.lv3);
                PlayerPrefs.SetInt("lv4", saveObject.lv4);
                PlayerPrefs.SetInt("lv5", saveObject.lv5);
                PlayerPrefs.SetInt("lv6", saveObject.lv6);

                Debug.Log("Loaded!");
            }
        }
    }

    // Save
    private void SaveUpdate(SavedGameRequestStatus status, ISavedGameMetadata meta)
    {
        Debug.Log(status);
    }

    private class SaveObject
    {
        public int score;
        public int exp;
        public bool buwana;
        public bool baskara;
        public bool kluwung;

        public int lv1, lv2, lv3, lv4, lv5, lv6;
    }
}
