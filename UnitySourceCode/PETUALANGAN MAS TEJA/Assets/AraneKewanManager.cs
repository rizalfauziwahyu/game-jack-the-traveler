﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AraneKewanManager : MonoBehaviour
{
    public GameObject pic1, pic2, pic3, pic4, pic5;
    private bool pic1done, pic2done, pic3done, pic4done, pic5done;
    public GameObject pic1black, pic2black, pic3black, pic4black, pic5black;
    private Vector2 pic1InitPos, pic2InitPos, pic3InitPos, pic4InitPos, pic5InitPos;
    public GameObject[] stars;
    public Sprite emptyStarSprite;
    public Sprite fullStarSprite;

    [Header("AUDIO CLIP")]
    public AudioClip correctSfx;
    public AudioClip incorrectSfx;
    public AudioClip winSfx;
    public AudioClip loseSfx;

    [Header("UI RESULT REF")]
    public GameObject[] resultStars;
    public GameObject resultCanvas;

    private int starCount = 3;

    private void Start()
    {
        pic1InitPos = pic1.transform.position;
        pic2InitPos = pic2.transform.position;
        pic3InitPos = pic3.transform.position;
        pic4InitPos = pic4.transform.position;
        pic5InitPos = pic5.transform.position;
    }

    private void UpdateStarImage()
    {
        int leftStar = 3 - starCount;

        for (int i = 0; i < leftStar; i++)
        {
            stars[i].gameObject.GetComponent<Image>().sprite = emptyStarSprite;
        }
    }

    public void DragPic1()
    {
        if (pic1done)
        {
            return;
        }
        pic1.transform.position = Input.mousePosition;
    }

    public void DragPic2()
    {
        if (pic2done)
        {
            return;
        }
        pic2.transform.position = Input.mousePosition;
    }

    public void DragPic3()
    {
        if (pic3done)
        {
            return;
        }
        pic3.transform.position = Input.mousePosition;
    }

    public void DragPic4()
    {
        if (pic4done)
        {
            return;
        }
        pic4.transform.position = Input.mousePosition;
    }

    public void DragPic5()
    {
        if (pic5done)
        {
            return;
        }
        pic5.transform.position = Input.mousePosition;
    }

    private void Lose()
    {
        SoundManager.instance.PlaySound(loseSfx);
        resultCanvas.SetActive(true);

        for (int i = 0; i < starCount; i++)
        {
            resultStars[i].gameObject.GetComponent<Image>().sprite = fullStarSprite;
        }

        PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, starCount);
        GameManager.instance.Save();
        GooglePlayManager.instance.OpenSave(true);
    }

    private void WinCondition()
    {
        if (
            pic1done &&
            pic2done &&
            pic3done &&
            pic4done &&
            pic5done
        )
        {
            SoundManager.instance.PlaySound(winSfx);
            resultCanvas.SetActive(true);
            for (int i = 0; i < starCount; i++)
            {
                resultStars[i].gameObject.GetComponent<Image>().sprite = fullStarSprite;
            }

            PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, starCount);
            GameManager.instance.Save();
            GooglePlayManager.instance.OpenSave(true);
        }
    }


    public void DropPic1()
    {
        float Distance = Vector3.Distance(pic1.transform.position, pic1black.transform.position);

        Debug.Log("drop " + Distance);

        if (Distance < 50)
        {
            pic1.transform.position = pic1black.transform.position;
            SoundManager.instance.PlaySound(correctSfx);
            pic1done = true;

            WinCondition();
        }
        else
        {
            pic1.transform.position = pic1InitPos;
            SoundManager.instance.PlaySound(incorrectSfx);

            if (starCount > 0)
            {
                starCount -= 1;
                UpdateStarImage();
            }
            else
            {
                Lose();
            }

            if (starCount == 0)
            {
                Lose();
            }
        }
    }

    public void DropPic2()
    {
        float distance = Vector3.Distance(pic2.transform.position, pic2black.transform.position);

        if (distance < 50)
        {
            pic2.transform.position = pic2black.transform.position;
            SoundManager.instance.PlaySound(correctSfx);
            pic2done = true;

            WinCondition();
        }
        else
        {
            pic2.transform.position = pic2InitPos;
            SoundManager.instance.PlaySound(incorrectSfx);

            if (starCount > 0)
            {
                starCount -= 1;
                UpdateStarImage();
            }
            else
            {
                Lose();
            }

            if (starCount == 0)
            {
                Lose();
            }
        }
    }

    public void DropPic3()
    {
        float distance = Vector3.Distance(pic3.transform.position, pic3black.transform.position);

        if (distance < 50)
        {
            pic3.transform.position = pic3black.transform.position;
            SoundManager.instance.PlaySound(correctSfx);
            pic3done = true;

            WinCondition();
        }
        else
        {
            pic3.transform.position = pic3InitPos;
            SoundManager.instance.PlaySound(incorrectSfx);

            if (starCount > 0)
            {
                starCount -= 1;
                UpdateStarImage();
            }
            else
            {
                Lose();
            }

            if (starCount == 0)
            {
                Lose();
            }
        }
    }

    public void DropPic4()
    {
        float distance = Vector3.Distance(pic4.transform.position, pic4black.transform.position);

        if (distance < 50)
        {
            pic4.transform.position = pic4black.transform.position;
            SoundManager.instance.PlaySound(correctSfx);
            pic4done = true;

            WinCondition();
        }
        else
        {
            pic4.transform.position = pic4InitPos;
            SoundManager.instance.PlaySound(incorrectSfx);

            if (starCount > 0)
            {
                starCount -= 1;
                UpdateStarImage();
            }
            else
            {
                Lose();
            }

            if (starCount == 0)
            {
                Lose();
            }
        }
    }

    public void DropPic5()
    {
        float distance = Vector3.Distance(pic5.transform.position, pic5black.transform.position);

        if (distance < 50)
        {
            pic5.transform.position = pic5black.transform.position;
            SoundManager.instance.PlaySound(correctSfx);
            pic5done = true;

            WinCondition();
        }
        else
        {
            pic5.transform.position = pic5InitPos;

            SoundManager.instance.PlaySound(incorrectSfx);

            if (starCount > 0)
            {
                starCount -= 1;
                UpdateStarImage();
            }
            else
            {
                Lose();
            }

            if (starCount == 0)
            {
                Lose();
            }
        }
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
}
