﻿using System;
using UnityEngine;
using UnityEngine.UI;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;

public class GameManager : MonoBehaviour
{
    public static GameManager instance { get; private set; }

    public MapSelection buwana;
    public MapSelection baskara;
    public MapSelection kluwung;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            SaveSystem.Init();
            GooglePlayManager.instance.OpenSave(false);

            //unlock map buwana
            PlayerPrefs.SetInt("buwanaStatus", 1);

            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Save()
    {
        SaveObject saveObject = new SaveObject
        {
            lv1 = PlayerPrefs.GetInt("lv1"),
            lv2 = PlayerPrefs.GetInt("lv2"),
            lv3 = PlayerPrefs.GetInt("lv3"),
            lv4 = PlayerPrefs.GetInt("lv4"),
            lv5 = PlayerPrefs.GetInt("lv5"),
            lv6 = PlayerPrefs.GetInt("lv6"),
            lv7 = PlayerPrefs.GetInt("lv7"),
            lv8 = PlayerPrefs.GetInt("lv8"),
            lv9 = PlayerPrefs.GetInt("lv9"),
            lv10 = PlayerPrefs.GetInt("lv10"),
            lv11 = PlayerPrefs.GetInt("lv11"),
            lv12 = PlayerPrefs.GetInt("lv12"),
            lv13 = PlayerPrefs.GetInt("lv13"),
            lv14 = PlayerPrefs.GetInt("lv14"),
            lv15 = PlayerPrefs.GetInt("lv15"),
            lv16 = PlayerPrefs.GetInt("lv16"),
        };

        string json = JsonUtility.ToJson(saveObject);

        SaveSystem.Save(json);
        Debug.Log("Saved!");
    }

    public void Load()
    {
        // Load
        string saveString = SaveSystem.Load();
        if (saveString != null)
        {
            SaveObject saveObject = JsonUtility.FromJson<SaveObject>(saveString);

            PlayerPrefs.SetInt("baskaraStatus", boolToInt(saveObject.baskaraStatus));
            PlayerPrefs.SetInt("kluwungStatus", boolToInt(saveObject.kluwungStatus));

            PlayerPrefs.SetInt("lv1", saveObject.lv1);
            PlayerPrefs.SetInt("lv2", saveObject.lv2);
            PlayerPrefs.SetInt("lv3", saveObject.lv3);
            PlayerPrefs.SetInt("lv4", saveObject.lv4);
            PlayerPrefs.SetInt("lv5", saveObject.lv5);
            PlayerPrefs.SetInt("lv6", saveObject.lv6);
            PlayerPrefs.SetInt("lv7", saveObject.lv7);
            PlayerPrefs.SetInt("lv8", saveObject.lv8);
            PlayerPrefs.SetInt("lv9", saveObject.lv9);
            PlayerPrefs.SetInt("lv10", saveObject.lv10);
            PlayerPrefs.SetInt("lv11", saveObject.lv11);
            PlayerPrefs.SetInt("lv12", saveObject.lv12);
            PlayerPrefs.SetInt("lv13", saveObject.lv13);
            PlayerPrefs.SetInt("lv14", saveObject.lv14);
            PlayerPrefs.SetInt("lv15", saveObject.lv15);
            PlayerPrefs.SetInt("lv16", saveObject.lv16);

            PlayerPrefs.SetInt("cerdasCermatHighScore", saveObject.cerdasCermatHighScore);



            Debug.Log("Loaded!");
        }
        else
        {
            Debug.Log("No Save!");
        }
    }

    int boolToInt(bool val)
    {
        if (val)
            return 1;
        else
            return 0;
    }

    private class SaveObject
    {
        public int score;
        public int exp;
        public bool buwanaStatus;
        public bool baskaraStatus;
        public bool kluwungStatus;
        public int buwana;
        public int baskara;
        public int kluwung;

        public int lv1, lv2, lv3, lv4, lv5, lv6, lv7, lv8;
        public int lv9, lv10, lv11, lv12, lv13, lv14, lv15, lv16;

        public int cerdasCermatHighScore;
    }
}
