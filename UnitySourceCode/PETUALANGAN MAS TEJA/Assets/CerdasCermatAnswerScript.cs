﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CerdasCermatAnswerScript : MonoBehaviour
{
    public bool isCorrect = false;
    public CerdasCermatManager quizManager;
    public void Answer()
    {
        if (isCorrect)
        {
            Debug.Log("Correct Answer");
            quizManager.Correct();
        }
        else 
        {
            Debug.Log("Wrong Answer");
            quizManager.InCorrect();
        }
    }
}
