﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{

    public const int gridRows = 2;
    public const int gridCols = 5;
    public const float offsetX = 3f;
    public const float offsetY = 4f;

    [Header("Audio Sfx")]
    public AudioClip correctSfx;
    public AudioClip incorrectSfx;
    public AudioClip winSfx;
    public AudioClip loseSfx;

    [Header("Result UI")]
    public GameObject resultCanvas;
    public GameObject[] stars;
    public Sprite fullStar;

    [SerializeField] private MainCard originalCard;
    [SerializeField] private Sprite[] images;

    private void Start()
    {
        Vector3 startPos = originalCard.transform.position; //The position of the first card. All other cards are offset from here.

        int[] numbers = { 0, 0, 1, 1, 2, 2, 3, 3, 4, 4 };
        numbers = ShuffleArray(numbers); //This is a function we will create in a minute!

        for (int i = 0; i < gridCols; i++)
        {
            for (int j = 0; j < gridRows; j++)
            {
                MainCard card;
                if (i == 0 && j == 0)
                {
                    card = originalCard;
                }
                else
                {
                    card = Instantiate(originalCard) as MainCard;
                }

                int index = j * gridCols + i;
                int id = numbers[index];
                card.ChangeSprite(id, images[id]);

                float posX = (offsetX * i) + startPos.x;
                float posY = (offsetY * j) + startPos.y;
                card.transform.position = new Vector3(posX, posY, startPos.z);
            }
        }

        timeLabel.text = Mathf.RoundToInt(timeLimit).ToString();
        StartCoroutine(TimeLimit());
    }

    private int[] ShuffleArray(int[] numbers)
    {
        int[] newArray = numbers.Clone() as int[];
        for (int i = 0; i < newArray.Length; i++)
        {
            int tmp = newArray[i];
            int r = Random.Range(i, newArray.Length);
            newArray[i] = newArray[r];
            newArray[r] = tmp;
        }
        return newArray;
    }

    //-------------------------------------------------------------------------------------------------------------------------------------------

    private MainCard _firstRevealed;
    private MainCard _secondRevealed;

    private int sumCardRevealed = 0;

    public float timeLimit = 40;
    [SerializeField] private TextMesh timeLabel;

    public bool canReveal
    {
        get { return _secondRevealed == null; }
    }

    public void CardRevealed(MainCard card)
    {
        if (_firstRevealed == null)
        {
            _firstRevealed = card;
        }
        else
        {
            _secondRevealed = card;
            StartCoroutine(CheckMatch());
        }
    }

    private IEnumerator CheckMatch()
    {
        if (_firstRevealed.id == _secondRevealed.id)
        {
            sumCardRevealed += 1;
            SoundManager.instance.PlaySound(correctSfx);
            WinCondition();
        }
        else
        {
            SoundManager.instance.PlaySound(incorrectSfx);
            yield return new WaitForSeconds(0.5f);

            _firstRevealed.Unreveal();
            _secondRevealed.Unreveal();
        }

        _firstRevealed = null;
        _secondRevealed = null;

    }

    private void WinCondition()
    {
        if (sumCardRevealed == 5)
        {
            resultCanvas.SetActive(true);
            SoundManager.instance.PlaySound(winSfx);
            float timeLeft = timeLimit;

            if (timeLeft > 20)
            {
                PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, 3);
            } else if (timeLeft > 14 && timeLeft < 20)
            {
                PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, 2);
            } else if (timeLeft > 5 && timeLeft < 15)
            {
                PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, 1);
            }

            UpdateStarImage();
            GameManager.instance.Save();
            GooglePlayManager.instance.OpenSave(true);
        }
    }

    private void LoseCondition()
    {
        resultCanvas.SetActive(true);
        SoundManager.instance.PlaySound(loseSfx);
    }

    IEnumerator TimeLimit()
    {
        timeLabel.text = Mathf.RoundToInt(timeLimit).ToString();

        yield return new WaitForSeconds(1);

        while (timeLimit > 0)
        {
            timeLimit -= Time.deltaTime;
            timeLabel.text = Mathf.RoundToInt(timeLimit).ToString();

            if (sumCardRevealed == 5)
            {
                yield break;
            }

            yield return null;
        }

        LoseCondition();
    }

    public void Restart()
    {
        SceneManager.LoadScene("Scene_001");
    }


    public void BackToMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public void UpdateStarImage()
    {
        for (int i = 0; i < PlayerPrefs.GetInt(SceneManager.GetActiveScene().name); i++)
        {
            stars[i].gameObject.GetComponent<Image>().sprite = fullStar;
        }
    }
}
