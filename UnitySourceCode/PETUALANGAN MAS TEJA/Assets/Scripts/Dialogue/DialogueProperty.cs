﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogueProperty
{
    public string name;
    public string sentence;
    public Sprite characterSprite;
}
