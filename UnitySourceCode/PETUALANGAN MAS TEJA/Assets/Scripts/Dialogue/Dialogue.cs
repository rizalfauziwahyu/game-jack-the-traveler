﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Dialogue 
{
    [Header ("Dialogue Property")]
    public DialogueProperty[] sentences;
}
