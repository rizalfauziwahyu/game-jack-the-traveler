﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogueManager : MonoBehaviour
{
    public Text nameText;
    public Text dialogueText;

    [Header("Time parameters")]
    [SerializeField] private float delay;
    [SerializeField] private float delayBetweenLines;

    [Header("Sound parameters")]
    [SerializeField] private AudioClip sound;

    [Header("Character Image")]
    [SerializeField] private Image imageHolder;
    private Queue<DialogueProperty> sentences;

    public string levelScene;
    void Awake()
    {
        sentences = new Queue<DialogueProperty>();
    }

    public void StartDialogue(Dialogue dialogue)
    {
        foreach (DialogueProperty sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            SceneManager.LoadScene(levelScene);
            EndDialogue();
            return;
        }

        DialogueProperty sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    IEnumerator TypeSentence(DialogueProperty sentence)
    {
        dialogueText.text = "";
        nameText.text = "";

        nameText.text = sentence.name;
        imageHolder.sprite = sentence.characterSprite;

        foreach (char letter in sentence.sentence.ToCharArray())
        {
            dialogueText.text += letter;
            SoundManager.instance.PlaySound(sound);
            yield return new WaitForSeconds(delay);
        }
    }

    void EndDialogue()
    {
        Debug.Log("End Of Conversation");
    }
}
