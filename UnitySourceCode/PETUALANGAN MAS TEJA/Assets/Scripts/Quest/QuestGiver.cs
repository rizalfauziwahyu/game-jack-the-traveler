﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuestGiver : MonoBehaviour
{
    public Player player;

    public Text titleText;
    public Text descriptionText;
    public Text exprerienceRewardText;
    public Text estimatedTimeText;

    private string levelScene;

    public void AssignQuest(QuestData quest)
    {
        // titleText.text = quest.title;
        descriptionText.text = quest.description;
        exprerienceRewardText.text = $"{quest.experience.ToString()} exp";
        estimatedTimeText.text = $"{quest.estimatedTime.ToString()} s";
        levelScene = quest.levelScene;
    }

    public void LevelSelection()
    {
        SceneManager.LoadScene(levelScene);
    }

}
