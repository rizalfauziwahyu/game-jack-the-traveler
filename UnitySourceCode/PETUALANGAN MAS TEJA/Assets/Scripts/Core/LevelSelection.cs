﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSelection : MonoBehaviour
{
    public GameObject[] stars;

    public Sprite starSprite;

    private void Awake()
    {
        // PlayerPrefs.DeleteAll();
    }

    private void Update()
    {
        UpdateLevelImage();//TODO MOve this method later
        // UpdateLevelStatus();//TODO MOve this method later
    }

    private void UpdateLevelImage()
    {
        for (int i = 0; i < PlayerPrefs.GetInt("lv" + gameObject.name); i++)
        {
            stars[i].gameObject.GetComponent<Image>().sprite = starSprite;
        }
    }

    public void PressSelection(string _LevelName)//When we press this level, we can move to the corresponding Scene to play
    {

        SceneManager.LoadScene(_LevelName);

    }
}
