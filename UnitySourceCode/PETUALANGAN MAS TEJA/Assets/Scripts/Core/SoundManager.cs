﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance { get; private set; }

    private AudioSource source;

    public AudioClip buttonSfx;
    private void Awake()
    {        
        if (instance == null)
        {
            instance = this;
            source = GetComponent<AudioSource>();

            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void PlaySound(AudioClip sound)
    {
        source.PlayOneShot(sound);
    }

    public void PlayButtonSound()
    {
        source.PlayOneShot(buttonSfx);
    }
}
