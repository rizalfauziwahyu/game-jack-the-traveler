﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MapSelection : MonoBehaviour
{
    [SerializeField] private bool unlocked;//Default value is false;
    public Image unlockImage;
    public GameObject[] stars;

    public GameObject openFrame;
    public GameObject currentFrame;

    public Sprite starSprite;

    public string mapname;

    private int starCount = 0;

    private void Start()
    {
        //PlayerPrefs.DeleteAll();

    }

    private void Update()
    {
        UpdateLevelImage();//TODO MOve this method later
        UpdateMapStatus();//TODO MOve this method later
    }

    bool intToBool(int val)
    {
        if (val != 0)
            return true;
        else
            return false;
    }

    public void UpdateMapStatus()
    {
        if (mapname == "buwana")
        {
            int lv1, lv2, lv3, lv4, lv5, lv6, lv7, lv8;
            lv1 = PlayerPrefs.GetInt("lv1");
            lv2 = PlayerPrefs.GetInt("lv2");
            lv3 = PlayerPrefs.GetInt("lv3");
            lv4 = PlayerPrefs.GetInt("lv4");
            lv5 = PlayerPrefs.GetInt("lv5");
            lv6 = PlayerPrefs.GetInt("lv6");
            lv7 = PlayerPrefs.GetInt("lv7");
            lv8 = PlayerPrefs.GetInt("lv8");

            int starSum = lv1 + lv2 + lv3 + lv4 + lv5 + lv6 + lv7 + lv8;

            if (starSum >= 24)
            {
                PlayerPrefs.SetInt("buwana", 3);
                PlayerPrefs.SetInt("baskaraStatus", 1);
                starCount = 3;
            }
            else if (starSum >= 16 && starSum < 23)
            {
                PlayerPrefs.SetInt("buwana", 2);
                starCount = 2;
            }
            else if (starSum >= 6 && starSum < 16)
            {
                PlayerPrefs.SetInt("buwana", 1);
                starCount = 1;
            }
            else
            {
                PlayerPrefs.SetInt("buwana", 0);
                starCount = 0;
            }
        }
        else if (mapname == "baskara")
        {
            unlocked = intToBool(PlayerPrefs.GetInt("baskaraStatus"));

            int lv9, lv10, lv11, lv12, lv13, lv14, lv15, lv16;

            lv9 = PlayerPrefs.GetInt("lv9");
            lv10 = PlayerPrefs.GetInt("lv10");
            lv11 = PlayerPrefs.GetInt("lv11");
            lv12 = PlayerPrefs.GetInt("lv12");
            lv13 = PlayerPrefs.GetInt("lv13");
            lv14 = PlayerPrefs.GetInt("lv14");
            lv15 = PlayerPrefs.GetInt("lv15");
            lv16 = PlayerPrefs.GetInt("lv16");

            int starSum = lv9 + lv10 + lv11 + lv12;

            if (starSum >= 24)
            {
                PlayerPrefs.SetInt("baskara", 3);
                PlayerPrefs.SetInt("kluwungStatus", 1);
                starCount = 3;
            }
            else if (starSum >= 16 && starSum < 24)
            {
                PlayerPrefs.SetInt("baskara", 2);
                starCount = 2;
            }
            else if (starSum >= 6 && starSum < 16)
            {
                PlayerPrefs.SetInt("baskara", 1);
                starCount = 1;
            }
            else
            {
                PlayerPrefs.SetInt("baskara", 0);
            }
        }
        else if (mapname == "kluwung")
        {
            unlocked = intToBool(PlayerPrefs.GetInt("kluwungStatus"));

            int lv17, lv18, lv19, lv20, lv21, lv22, lv23, lv24;
            lv17 = PlayerPrefs.GetInt("lv17");
            lv18 = PlayerPrefs.GetInt("lv18");
            lv19 = PlayerPrefs.GetInt("lv19");
            lv20 = PlayerPrefs.GetInt("lv20");
            lv21 = PlayerPrefs.GetInt("lv21");
            lv22 = PlayerPrefs.GetInt("lv22");
            lv23 = PlayerPrefs.GetInt("lv23");
            lv24 = PlayerPrefs.GetInt("lv24");

            int starSum = lv17 + lv18 + lv19 + lv20 + lv21 + lv22 + lv23 + lv24;

            if (starSum >= 24)
            {
                PlayerPrefs.SetInt("kluwung", 3);
                starCount = 3;
            }
            else if (starSum >= 16 && starSum < 24)
            {
                PlayerPrefs.SetInt("kluwung", 2);
                starCount = 2;
            }
            else if (starSum >= 6 && starSum < 16)
            {
                PlayerPrefs.SetInt("kluwung", 1);
                starCount = 1;
            }
            else
            {
                PlayerPrefs.SetInt("kluwung", 0);
            }
        }
    }

    private void UpdateLevelImage()
    {
        if (!unlocked)//MARKER if unclock is false means This level is clocked!
        {
            unlockImage.gameObject.SetActive(true);
            for (int i = 0; i < stars.Length; i++)
            {
                stars[i].gameObject.SetActive(false);
            }
        }
        else//if unlock is true means This level can play !
        {
            unlockImage.gameObject.SetActive(false);
            for (int i = 0; i < stars.Length; i++)
            {
                stars[i].gameObject.SetActive(true);
            }

            for (int i = 0; i < starCount; i++)
            {
                stars[i].gameObject.GetComponent<Image>().sprite = starSprite;
            }
        }
    }

    public void PressSelection()//When we press this level, we can move to the corresponding Scene to play
    {
        if (unlocked)
        {
            SoundManager.instance.PlayButtonSound();
            LeanTween.moveLocal(currentFrame, new Vector3(0, 1200, 0), 0.5f).setEaseSpring();
            LeanTween.moveLocal(openFrame, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
        }
    }
}
