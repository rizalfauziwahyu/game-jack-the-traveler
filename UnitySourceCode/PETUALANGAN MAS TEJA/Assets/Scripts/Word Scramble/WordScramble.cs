﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Word
{
    public string word;
    [Space(10)]
    public float timeLimit;
    [Header("leave empty if you want randomized")]
    public string desiredRandom;
    [Header("Question")]
    public string question;


    public string GetString()
    {
        if (!string.IsNullOrEmpty(desiredRandom))
        {
            return desiredRandom;
        }

        string result = word;

        List<char> characters = new List<char>(word.ToCharArray());

        while (result == word)
        {
            result = "";
            while (characters.Count > 0)
            {
                int indexChar = Random.Range(0, characters.Count - 1);
                result += characters[indexChar];

                characters.RemoveAt(indexChar);
            }
        }


        while (characters.Count > 0)
        {
            int indexChar = Random.Range(0, characters.Count - 1);
            result += characters[indexChar];

            characters.RemoveAt(indexChar);
        }

        return result;
    }

}

[System.Serializable]
public class Result
{
    public int totalScore = 0;

    [Header("REF UI")]
    public Text textTime;
    public Text textTotalScore;

    [Header("REF RESULT SCREEN")]
    public GameObject resultCanvas;
    public GameObject star1;
    public GameObject star2;
    public GameObject star3;
    public Text textResultScore;

    private int starCount = 0;

    public void ShowResult()
    {
        textResultScore.text = totalScore.ToString();

        int allTimeLimit = WordScramble.main.GetAllTimeLimit();

        if (totalScore <= (allTimeLimit / 3))
        {
            star1.SetActive(true);
            starCount = 1;
        }
        else if (totalScore <= (allTimeLimit / 2))
        {
            star2.SetActive(true);
            starCount = 2;
        }
        else
        {
            star3.SetActive(true);
            starCount = 3;
            levelClear();
        }

        PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, starCount);
        resultCanvas.SetActive(true);
        GameManager.instance.Save();
        GooglePlayManager.instance.OpenSave(true);
    }

    public void levelClear()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        if (sceneName == "lv1")
        {
            GooglePlayManager.instance.UnlockAchievement(GPGSIds.achievement_buwana_1);
        }
        else if (sceneName == "lv2")
        {
            GooglePlayManager.instance.UnlockAchievement(GPGSIds.achievement_buwana_2);
        }
    }

}
public class WordScramble : MonoBehaviour
{
    public Word[] words;
    [Space(10)]
    public Result result;

    public float timeLimit;

    [Header("UI Reference")]
    public GameObject wordCanvas;
    public CharObject prefab;
    public CharObject firstSelected;
    public Transform container;
    public Text question;
    public float space;
    public float lerpSpeed;
    
    public AudioClip buttonSfx;

    List<CharObject> charObjects = new List<CharObject>();

    public int currentWord;

    public static WordScramble main;

    private float totalScore;

    private void Awake()
    {
        main = this;
    }

    public int GetAllTimeLimit()
    {
        float result = 0;
        foreach (Word w in words)
        {
            result += w.timeLimit / 2;
        }

        return Mathf.RoundToInt(result);
    }

    void RespositionObject()
    {
        if (charObjects.Count == 0)
        {
            return;
        }

        float center = (charObjects.Count - 1) / 2;
        for (int i = 0; i < charObjects.Count; i++)
        {
            charObjects[i].rectTransform.anchoredPosition = Vector2.Lerp(charObjects[i].rectTransform.anchoredPosition, new Vector2((i - center) * space, 0), lerpSpeed * Time.deltaTime);

            charObjects[i].index = i;
        }
    }

    /// <summary>
    /// Show a random word to the screen
    /// </summary>
    public void ShowScramble()
    {
        ShowScramble(Random.Range(0, words.Length - 1));
    }

    /// <summary>
    /// Show word from collection with desired index
    /// </summary>
    public void ShowScramble(int index)
    {
        charObjects.Clear();
        foreach (Transform child in container)
        {
            Destroy(child.gameObject);
        }

        if (index > words.Length - 1)
        {
            result.ShowResult();
            wordCanvas.SetActive(false);
            // Debug.LogError("index out of range, please enter range berween 0 -" + (words.Length - 1).ToString());
            return;
        }

        char[] chars = words[index].GetString().ToCharArray();
        foreach (char c in chars)
        {
            CharObject clone = Instantiate(prefab.gameObject).GetComponent<CharObject>();
            clone.transform.SetParent(container);

            charObjects.Add(clone.Init(c));
        }

        currentWord = index;
        question.text = words[index].question;
        StartCoroutine(TimeLimit());
    }

    public void Swap(int indexA, int indexB)
    {
        CharObject tmpA = charObjects[indexA];

        charObjects[indexA] = charObjects[indexB];
        charObjects[indexB] = tmpA;

        charObjects[indexA].transform.SetAsLastSibling();
        charObjects[indexB].transform.SetAsLastSibling();

        CheckWord();
    }

    public void Select(CharObject charObject)
    {
        if (firstSelected)
        {
            Swap(firstSelected.index, charObject.index);

            // Unselect
            firstSelected.Select();
            charObject.Select();
        }
        else
        {
            firstSelected = charObject;
        }

        SoundManager.instance.PlaySound(buttonSfx);
    }

    public void CheckWord()
    {
        StartCoroutine(CoCheckWord());
    }

    public void Unselect()
    {
        firstSelected = null;
    }

    IEnumerator CoCheckWord()
    {
        yield return new WaitForSeconds(0.5f);
        string word = "";
        foreach (CharObject charObject in charObjects)
        {
            word += charObject.character;
        }

        if (timeLimit <= 0)
        {
            currentWord++;
            ShowScramble(currentWord);
            yield break;
        }

        if (word == words[currentWord].word)
        {
            currentWord++;
            result.totalScore += Mathf.RoundToInt(timeLimit);
            ShowScramble(currentWord);
        }
    }

    IEnumerator TimeLimit()
    {
        timeLimit = words[currentWord].timeLimit;
        result.textTime.text = Mathf.RoundToInt(timeLimit).ToString() + " detik";
        int myWord = currentWord;
        yield return new WaitForSeconds(1);

        while (timeLimit > 0)
        {
            if (myWord != currentWord) { yield break; }
            timeLimit -= Time.deltaTime;
            result.textTime.text = Mathf.RoundToInt(timeLimit).ToString() + " detik";

            yield return null;
        }

        CheckWord();
    }
    // Start is called before the first frame update
    void Start()
    {
        ShowScramble(currentWord);
        result.textTotalScore.text = result.totalScore.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        RespositionObject();

        totalScore = Mathf.Lerp(totalScore, result.totalScore, Time.deltaTime * 3);
        result.textTotalScore.text = Mathf.RoundToInt(totalScore).ToString();
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
}
