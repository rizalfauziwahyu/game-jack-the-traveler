﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public GameObject settingMenu, creditMenu, pepakMenu, cerdasCermatMenu, storyModeMenu, mainMenuPanel, buwanaFrame, baskaraFrame, kluwungFrame, questFrame, prologueFrame;

    public AudioClip buttonSfx;
    public AudioClip backButtonSfx;
    public void OpenSetting()
    {
        SoundManager.instance.PlaySound(buttonSfx);
        LeanTween.moveLocal(settingMenu, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
        LeanTween.scale(mainMenuPanel, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
    }

    public void CloseSetting()
    {
        SoundManager.instance.PlaySound(backButtonSfx);
        LeanTween.moveLocal(settingMenu, new Vector3(1880, 0, 0), 0.5f).setEaseSpring();
        LeanTween.scale(mainMenuPanel, new Vector3(1, 1, 0), 0.5f).setEaseSpring();
    }

    public void OpenCredit()
    {
        SoundManager.instance.PlaySound(buttonSfx);
        LeanTween.moveLocal(creditMenu, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
        LeanTween.scale(mainMenuPanel, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
    }

    public void CloseCredit()
    {
        SoundManager.instance.PlaySound(backButtonSfx);
        LeanTween.moveLocal(creditMenu, new Vector3(1880, 0, 0), 0.5f).setEaseSpring();
        LeanTween.scale(mainMenuPanel, new Vector3(1, 1, 0), 0.5f).setEaseSpring();
    }

    public void OpenStoryMode()
    {
        SoundManager.instance.PlaySound(buttonSfx);
        LeanTween.moveLocal(storyModeMenu, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
        LeanTween.scale(mainMenuPanel, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
    }

    public void CloseStoryMode()
    {
        SoundManager.instance.PlaySound(backButtonSfx);
        LeanTween.moveLocal(storyModeMenu, new Vector3(0, 1200, 0), 0.5f).setEaseSpring();
        LeanTween.scale(mainMenuPanel, new Vector3(1, 1, 0), 0.5f).setEaseSpring();
    }

    public void OpenBuwanaFrame()
    {
        SoundManager.instance.PlaySound(buttonSfx);
        LeanTween.moveLocal(storyModeMenu, new Vector3(0, 1200, 0), 0.5f).setEaseSpring();
        LeanTween.moveLocal(buwanaFrame, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
    }

    public void CloseBuwanaFrame()
    {
        SoundManager.instance.PlaySound(backButtonSfx);
        LeanTween.moveLocal(buwanaFrame, new Vector3(0, 1200, 0), 0.5f).setEaseSpring();
        LeanTween.moveLocal(storyModeMenu, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
    }

    public void OpenBaskaraFrame()
    {
        SoundManager.instance.PlaySound(buttonSfx);
        LeanTween.moveLocal(storyModeMenu, new Vector3(0, 1200, 0), 0.5f).setEaseSpring();
        LeanTween.moveLocal(baskaraFrame, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
    }

    public void CloseBaskaraFrame()
    {
        SoundManager.instance.PlaySound(backButtonSfx);
        LeanTween.moveLocal(baskaraFrame, new Vector3(0, 1200, 0), 0.5f).setEaseSpring();
        LeanTween.moveLocal(storyModeMenu, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
    }

    public void OpenKluwungFrame()
    {
        SoundManager.instance.PlaySound(buttonSfx);
        LeanTween.moveLocal(storyModeMenu, new Vector3(0, 1200, 0), 0.5f).setEaseSpring();
        LeanTween.moveLocal(kluwungFrame, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
    }

    public void CloseKluwungFrame()
    {
        SoundManager.instance.PlaySound(backButtonSfx);
        LeanTween.moveLocal(kluwungFrame, new Vector3(0, 1200, 0), 0.5f).setEaseSpring();
        LeanTween.moveLocal(storyModeMenu, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
    }

    public void OpenQuestFrame()
    {
        SoundManager.instance.PlaySound(buttonSfx);
        LeanTween.moveLocal(questFrame, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
    }

    public void CloseQuestFrame()
    {
        SoundManager.instance.PlaySound(backButtonSfx);
        LeanTween.moveLocal(questFrame, new Vector3(0, 1200, 0), 0.5f).setEaseSpring();
    }

    public void OpenPrologFrame()
    {
        SoundManager.instance.PlaySound(buttonSfx);
        LeanTween.scale(prologueFrame, new Vector3(1, 1, 1), 0.5f);
    }

    public void AcceptQuest(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void OpenCerdasCermat()
    {
        SoundManager.instance.PlaySound(buttonSfx);
        SceneManager.LoadScene("Cerdas Cermat");
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public void OpenPepak()
    {
        SoundManager.instance.PlayButtonSound();
        SceneManager.LoadScene("Pepak");
    }
}
