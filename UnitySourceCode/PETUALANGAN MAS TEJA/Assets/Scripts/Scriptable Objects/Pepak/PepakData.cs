﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Pepak Data", menuName = "Pepak")]
public class PepakData : ScriptableObject
{
    public string nameIndo;
    public string nameJawa;
    public Sprite image;

    public AudioClip indoSfx;
    public AudioClip jawaSfx;
}
