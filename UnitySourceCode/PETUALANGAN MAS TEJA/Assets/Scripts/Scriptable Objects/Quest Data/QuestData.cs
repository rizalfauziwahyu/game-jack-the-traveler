﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Quest", menuName = "Quest")]
public class QuestData : ScriptableObject
{
    public string title;
    public string description;
    public int experience;

    public int estimatedTime;

    public bool isActive;
    public string levelScene;

    public QuestGoal goal;
}
