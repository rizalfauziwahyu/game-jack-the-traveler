﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PepakButton : MonoBehaviour
{
    public Text btnText;

    public PepakData data;
    public PepakManager pepak;

    public GameObject detailPanel;

    private void Awake() {
        btnText.text = data.nameIndo;
    }

    public void OpenPanel()
    {
        pepak.SetIndoSfx(data.indoSfx);
        pepak.SetJawaSfx(data.jawaSfx);
        pepak.image.sprite = data.image;
        pepak.indoText.text = data.nameIndo;
        pepak.jawaText.text = data.nameJawa;

        LeanTween.moveLocal(detailPanel, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
    }

}
