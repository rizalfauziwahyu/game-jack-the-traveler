﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CerdasCermatManager : MonoBehaviour
{
    public List<QuestionAndAnswer> QnA;
    public GameObject[] options;
    public int currentQuestion;

    public CerdasCermatResult result;

    public float timeLimit;

    public Text QuestionText;

    public Text scoreText;
    public Text highscoreText;

    public AudioClip buttonSfxWrong;
    public AudioClip buttonSfxCorrect;
    public AudioClip resultCanvasSfx;

    private int allTimeLimit;


    private void Start()
    {
        GenerateQuestion();
        allTimeLimit = GetAllTimeLimit();
        highscoreText.text = PlayerPrefs.GetInt("cerdasCermatHighScore").ToString();
    }

    public void Correct()
    {
        SoundManager.instance.PlaySound(buttonSfxCorrect);
        result.totalScore += Mathf.RoundToInt(timeLimit);
        result.textTotalScore.text = result.totalScore.ToString();
        scoreText.text = result.totalScore.ToString();

        if (result.totalScore > PlayerPrefs.GetInt("cerdasCermatHighScore"))
        {
            highscoreText.text = result.totalScore.ToString();
        }

        QnA.RemoveAt(currentQuestion);
        GenerateQuestion();
    }

    public void InCorrect()
    {
        SoundManager.instance.PlaySound(buttonSfxWrong);
        QnA.Clear();

        if (result.totalScore > PlayerPrefs.GetInt("cerdasCermatHighScore"))
        {
            PlayerPrefs.SetInt("cerdasCermatHighScore", result.totalScore);
            GooglePlayManager.instance.ReportScore(result.totalScore);
        }
        result.resultCanvas.SetActive(true);
        GameManager.instance.Save();
    }

    void SetAnswer()
    {
        for (int i = 0; i < options.Length; i++)
        {
            options[i].GetComponent<CerdasCermatAnswerScript>().isCorrect = false;
            options[i].transform.GetChild(0).GetComponent<Text>().text = QnA[currentQuestion].Answers[i];

            if (QnA[currentQuestion].CorrectAnswer == i)
            {
                Debug.Log("Correct Answer : " + QnA[currentQuestion].Answers[i]);
                options[i].GetComponent<CerdasCermatAnswerScript>().isCorrect = true;
            }
        }
    }

    void GenerateQuestion()
    {
        if (QnA.Count > 0)
        {
            currentQuestion = Random.Range(0, QnA.Count);

            QuestionText.text = QnA[currentQuestion].Question;
            SetAnswer();
            StartCoroutine(TimeLimit());
        }
        else
        {
            SoundManager.instance.PlaySound(resultCanvasSfx);

            if (result.totalScore > PlayerPrefs.GetInt("cerdasCermatHighScore"))
            {
                PlayerPrefs.SetInt("cerdasCermatHighScore", result.totalScore);
            }
            result.resultCanvas.SetActive(true);
            GameManager.instance.Save();
        }

    }

    IEnumerator TimeLimit()
    {
        timeLimit = QnA[currentQuestion].time;
        result.textTime.text = Mathf.RoundToInt(timeLimit).ToString();
        int myQuestion = currentQuestion;
        yield return new WaitForSeconds(1);

        while (timeLimit > 0)
        {
            if (myQuestion != currentQuestion) { yield break; }
            timeLimit -= Time.deltaTime;
            result.textTime.text = Mathf.RoundToInt(timeLimit).ToString();

            yield return null;
        }

        if (QnA.Count > 0)
        {
            SoundManager.instance.PlaySound(buttonSfxWrong);
            QnA.Clear();
            GenerateQuestion();
        }
    }

    public int GetAllTimeLimit()
    {
        float result = 0;

        foreach (QuestionAndAnswer a in QnA)
        {
            result += a.time;
        }
        return Mathf.RoundToInt(result);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
}

[System.Serializable]
public class CerdasCermatResult
{
    public int totalScore = 0;

    [Header("REF UI")]
    public Text textTime;
    public Text textTotalScore;

    [Header("REF RESULT SCREEN")]
    public GameObject resultCanvas;

}