﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PepakManager : MonoBehaviour
{
    private AudioClip indoSfx;
    private AudioClip jawaSfx;

    public Text indoText;
    public Text jawaText;

    public Image image;

    public GameObject araneKewanPanel, peranganeAwakPanel; 
    public GameObject detailPanel;

    public void PlayIndoSfx()
    {
        SoundManager.instance.PlaySound(indoSfx);
    }

    public void PlayJawaSfx()
    {
        SoundManager.instance.PlaySound(jawaSfx);
    }

    public void Back()
    {
        LeanTween.moveLocal(detailPanel, new Vector3(0, 2075, 0), 0.5f).setEaseSpring();
    }

    public void SetIndoSfx(AudioClip sfx)
    {
        indoSfx = sfx;
    }

    public void SetJawaSfx(AudioClip sfx)
    {
        jawaSfx = sfx;
    }


    // Helper
    public void OpenAraneKewanPanel()
    {
        LeanTween.moveLocal(araneKewanPanel, new Vector3(0, 0, 0), 0.5f).setEaseSpring();
    }

    public void CloseAraneKewanPanel()
    {
        LeanTween.moveLocal(araneKewanPanel, new Vector3(2075, 0, 0), 0.5f).setEaseSpring();
    }

    public void OpenPeranganeAwakPanel()
    {
        LeanTween.moveLocal(peranganeAwakPanel, new Vector3(2075, 0, 0), 0.5f).setEaseSpring();
    }

    public void ClosePeranganeAwakPanel()
    {
        LeanTween.moveLocal(peranganeAwakPanel, new Vector3(2075, 0, 0), 0.5f).setEaseSpring();
    }


}
