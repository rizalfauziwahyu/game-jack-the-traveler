﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuizManager : MonoBehaviour
{
    public List<QuestionAndAnswer> QnA;
    public GameObject[] options;
    public int currentQuestion;

    public QuizResult result;

    public float timeLimit;

    public Text QuestionText;

    public Text scoreText;

    public AudioClip buttonSfxWrong;
    public AudioClip buttonSfxCorrect;
    public AudioClip resultCanvasSfx;

    private int allTimeLimit;


    private void Start()
    {
        GenerateQuestion();
        allTimeLimit = GetAllTimeLimit();
    }

    public void Correct()
    {
        SoundManager.instance.PlaySound(buttonSfxCorrect);
        result.totalScore += Mathf.RoundToInt(timeLimit);
        result.textTotalScore.text = result.totalScore.ToString();
        scoreText.text = result.totalScore.ToString();
        QnA.RemoveAt(currentQuestion);
        GenerateQuestion();
    }

    public void InCorrect()
    {
        SoundManager.instance.PlaySound(buttonSfxWrong);
        QnA.RemoveAt(currentQuestion);
        GenerateQuestion();
    }

    void SetAnswer()
    {
        for (int i = 0; i < options.Length; i++)
        {
            options[i].GetComponent<AnswerScript>().isCorrect = false;
            options[i].transform.GetChild(0).GetComponent<Text>().text = QnA[currentQuestion].Answers[i];

            if (QnA[currentQuestion].CorrectAnswer == i)
            {
                Debug.Log("Correct Answer : " + QnA[currentQuestion].Answers[i]);
                options[i].GetComponent<AnswerScript>().isCorrect = true;
            }
        }
    }

    void GenerateQuestion()
    {
        if (QnA.Count > 0)
        {
            currentQuestion = Random.Range(0, QnA.Count);

            QuestionText.text = QnA[currentQuestion].Question;
            SetAnswer();
            StartCoroutine(TimeLimit());
        }
        else
        {
            SoundManager.instance.PlaySound(resultCanvasSfx);
            GooglePlayManager.instance.UnlockAchievement(GPGSIds.achievement_buwana_3);

            if (result.totalScore <= (allTimeLimit / 2))
            {
                PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, 0);
            }
            else if (result.totalScore <= (allTimeLimit / 1.5))
            {
                PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, 2);
            }
            else
            {
                PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, 3);
            }
            result.UpdateStarImage();
            result.resultCanvas.SetActive(true);
            GameManager.instance.Save();
        }

    }

    IEnumerator TimeLimit()
    {
        timeLimit = QnA[currentQuestion].time;
        result.textTime.text = Mathf.RoundToInt(timeLimit).ToString();
        int myQuestion = currentQuestion;
        yield return new WaitForSeconds(1);

        while (timeLimit > 0)
        {
            if (myQuestion != currentQuestion) { yield break; }
            timeLimit -= Time.deltaTime;
            result.textTime.text = Mathf.RoundToInt(timeLimit).ToString();

            yield return null;
        }

        if (QnA.Count > 0)
        {
            SoundManager.instance.PlaySound(buttonSfxWrong);
            QnA.RemoveAt(currentQuestion);
            GenerateQuestion();
        }
    }

    public int GetAllTimeLimit()
    {
        float result = 0;

        foreach (QuestionAndAnswer a in QnA)
        {
            result += a.time;
        }
        return Mathf.RoundToInt(result);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
}

[System.Serializable]
public class QuizResult
{
    public int totalScore = 0;

    [Header("REF UI")]
    public Text textTime;
    public Text textTotalScore;

    public GameObject[] stars;

    public Sprite starSprite;

    [Header("REF RESULT SCREEN")]
    public GameObject resultCanvas;

    public void ShowResult()
    {

    }

    public void LevelClear()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        if (sceneName == "lv3")
        {
            GooglePlayManager.instance.UnlockAchievement(GPGSIds.achievement_buwana_3);
        }
        else if (sceneName == "lv4")
        {
            GooglePlayManager.instance.UnlockAchievement(GPGSIds.achievement_buwana_4);
        }
        else if (sceneName == "lv5")
        {
            GooglePlayManager.instance.UnlockAchievement(GPGSIds.achievement_buwana_5);
        }
        else if (sceneName == "lv6")
        {
            GooglePlayManager.instance.UnlockAchievement(GPGSIds.achievement_buwana_6);
        }
    }

    public void UpdateStarImage()
    {
        for (int i = 0; i < PlayerPrefs.GetInt(SceneManager.GetActiveScene().name); i++)
        {
            stars[i].gameObject.GetComponent<Image>().sprite = starSprite;
        }
    }
}